#!../../bin/linux-x86_64/tpg

## You may have to change tpg to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/tpg.dbd"
tpg_registerRecordDeviceDriver pdbbase

epicsEnvSet ("STREAM_PROTOCOL_PATH", ".")

#drvAsynIPPortConfigure("L2","172.17.10.63:8000",0,0,0)
drvAsynIPPortConfigure("L2","$(DEVIP):$(DEVPORT)",0,0,0)

asynOctetSetInputEos("L2",0,"\r\n")
asynOctetSetOutputEos("L2",0,"")


## Load record instances
dbLoadRecords("db/devTPG366.db" "BL=$(IOCBL),TPG366=$(IOCDEV),PORT=L2")
#dbLoadRecords("$(ASYN)/db/asynRecord.db" "P=PINK:MAXC:,R=asyn,PORT=L2,ADDR=0,OMAX=256,IMAX=256")

cd "${TOP}/iocBoot/${IOC}"

set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")

iocInit

create_monitor_set("auto_settings.req", 30, "BL=$(IOCBL),DEV=$(IOCDEV)")

## Start any sequence programs
#seq sncxxx,"user=epics"
